/** Subscriber Main Function.
 \file subscriber.cpp
 \author Fred Livingston
 \date 28 Feb 2019
 \version 0.1
 \brief Sample Code of using Garvity Subscribition Service
 \copyright Copyright(C) 2019 MechaSpin. All Rights Reserved.                                                            **
 */

#include <iostream>
#include <GravityNode.h>
#include <GravityLogger.h>
#include <GravitySubscriber.h>
#include <Utility.h>
#include "CLAMGravitySubscriber.h"

#define HEART_BEAT_INTERVAL 500000

int main()
{
	GravityNode gn;
	//Initialize gravity, giving this node a componentID.
	gn.init("CLAM_Gravity_Node");

	//Start a heartbeat that other components can listen to, telling them we're alive.
	gn.startHeartbeat(HEART_BEAT_INTERVAL);

	//Declare an object of type CLAMGravityCounterSubscriber
	CLAMGravitySubscriber clamSubscriber;
	
    //Subscribe a SimpleGravityCounterSubscriber to the counter data product.
	gn.subscribe("MooringPoseDataProduct", clamSubscriber);

	//Wait for us to exit (Ctrl-C or being killed).
	gn.waitForExit();

	//Currently this will never be hit because we will have been killed (unfortunately).
	//But this shouldn't make a difference because the OS should close the socket and free all resources.
	gn.unsubscribe("MooringPoseDataProduct", clamSubscriber);
}


