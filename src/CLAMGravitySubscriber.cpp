/** Subscriber Callback Function.
 \file CLAMGravitySubscriber.cpp
 \author Fred Livingston
 \date 28 Feb 2019
 \version 0.1
 \brief Sample Code of using Garvity Subscribition Service
 \copyright Copyright(C) 2019 MechaSpin. All Rights Reserved.                                                            **
 */
 
#include "CLAMGravitySubscriber.h"
#include <MooringPoseDataProduct.pb.h>

using namespace gravity;
using namespace std;


void CLAMGravitySubscriber::subscriptionFilled(const std::vector< std::shared_ptr<GravityDataProduct> >& dataProducts)
{
	for(std::vector< std::shared_ptr<GravityDataProduct> >::const_iterator i = dataProducts.begin();
			i != dataProducts.end(); i++)
	{
		//Get the protobuf object from the message
		MooringPoseDataProductPB mooringPoseDataPB;
		 (*i)->populateMessage(mooringPoseDataPB);

		//Process the message
		Log::warning("Current Morring Station <Px, Py, Pz>  <%d, %d, %d>", 
			mooringPoseDataPB.x_mm(), mooringPoseDataPB.y_mm(), mooringPoseDataPB.z_mm());
           
	}
}
