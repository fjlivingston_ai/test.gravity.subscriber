/** Subscriber CallBack Header.
 \file CLAMGravitySubscriber.h
 \author Fred Livingston
 \date 28 Feb 2019
 \version 0.1
 \brief Sample Code of using Garvity Subscribition Service
 \copyright Copyright(C) 2019 MechaSpin. All Rights Reserved.                                                            **
 */



#ifndef CLAMGRAVITYSUBSCRIBER_H
#define CLAMGRAVITYSUBSCRIBER_H

#include <iostream>
#include <GravityNode.h>
#include <GravityLogger.h>
#include <GravitySubscriber.h>
#include <Utility.h>


using namespace gravity;
using namespace std;

//Declare class for receiving Published messages.
class CLAMGravitySubscriber : public GravitySubscriber
{
public:
	virtual void subscriptionFilled(const std::vector< std::shared_ptr<GravityDataProduct> >& dataProducts);
};


#endif


